package com.puthisastra.mikeassignment;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class MainController {
	
	@Autowired
	private BookRepository bookrepository;
	
	@GetMapping
    @ResponseBody
	public List<Book> getAllBooks() {
		return bookrepository.findAll();
	}
	
	@GetMapping("/create")
	@ResponseBody
	public Book createBook() {
		Book book = new Book();
		book.setTitle("Mike");
		book.setPages(50);
		return bookrepository.save(book);
	}
	
	@GetMapping("/getBookById")
	@ResponseBody
	public Optional<Book> getBookById() {
		return bookrepository.findById(1L);
	}
	
	@GetMapping("/update")
	@ResponseBody
	public Book updateBookById() {
		Book book = bookrepository.getOne(1L);
		book.setTitle("w");
		return bookrepository.save(book);
	}
	
	@GetMapping("/delete")
	@ResponseBody
	public List<Book> deleteBookById() {
		bookrepository.deleteById(1L);
		return bookrepository.findAll();
	}
	
	//Assignment2
	@GetMapping("/getpagesgreaterthan")
    @ResponseBody
    public List<Book> getPagesGreaterthan() {
		return bookrepository.findByPagesGreaterThan(100);
    }
	
	@GetMapping("/gettitle")
	@ResponseBody
	public List<Book> getTitle() {
		return bookrepository.findByTitle("Mike");
	}
	
	@GetMapping("/gettitleandpagesequal")
	@ResponseBody
	public List<Book> getTitleAndPagesequal() {
		return bookrepository.findByTitleAndPages("Mike", 50);
	}
	
	@GetMapping("/getpagesbetween")
	@ResponseBody
	public List<Book> getPagesbetween() {
		return bookrepository.findByPagesBetween(50, 100);
	}
	
	@GetMapping("/getmaxnumberofpages")
	@ResponseBody
	public Integer GetMaxNumberOfPages() {
		return bookrepository.findByPagesQueryMax();
	}
	
	@GetMapping("/getbookshortbytitle")
	@ResponseBody
	public List<Book> GetBookSortByTitle() {
		return bookrepository.findByTitleQuerySort();
	}
	
	@GetMapping("/getbookjpqnameorpages")
	@ResponseBody
	public List<Book> GetBookJpqNameOrPages() {
		return bookrepository.findByTitleOrPages("Mike", 50);
	}
}

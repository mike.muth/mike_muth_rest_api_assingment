package com.puthisastra.mikeassignment;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BookRepository extends JpaRepository<Book, Long> {
	
	
	List<Book> findByPagesGreaterThan(Integer pages);
	
	List<Book> findByTitle(String title);

	List<Book> findByTitleAndPages(String title, Integer number);
	
	List<Book> findByPagesBetween(Integer pages1, Integer pages2);
	
	@Query("select MAX(b.pages) from Book b")
	Integer findByPagesQueryMax();
	
	@Query("select b from Book b order by b.title desc")
	List<Book> findByTitleQuerySort();
	
	@Query("select b from Book b where b.title = :title or b.pages= :pages")
	List<Book> findByTitleOrPages(@Param("title") String title, @Param("pages") Integer pages);
	
}

package com.puthisastra.mikeassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MikeassignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(MikeassignmentApplication.class, args);
	}

}
